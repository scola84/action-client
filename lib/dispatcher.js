'use strict';

class Dispatcher {
  constructor(actions, messenger) {
    this.actions = actions;
    this.messenger = messenger;
  }

  get(name) {
    if (!this.actions[name]) {
      throw new Error('action_not_found', {
        detail: {
          name
        }
      });
    }

    return this.actions[name]
      .get()
      .setMessenger(this.messenger)
      .setName(name);
  }
}

module.exports = Dispatcher;
