'use strict';

const Error = require('@scola/error');
const EventHandler = require('@scola/events');

class AbstractAction extends EventHandler {
  constructor(validator) {
    super();

    this.validator = validator;
    this.messenger = null;

    this.properties = {
      bind: null,
      data: null,
      hash: null,
      name: null
    };

    this.events = new Map();
  }

  destroy() {
    this.messenger.emit('debug', this, 'destroy');

    this.events.forEach((events, context) => {
      this.unlisten(context);
    });

    this.messenger.deleteAction(this);
    return this;
  }

  getMessenger() {
    return this.messenger;
  }

  setMessenger(messenger) {
    this.messenger = messenger;
    return this;
  }

  getName() {
    return this.properties.name;
  }

  setName(name) {
    this.properties.name = name;
    return this;
  }

  getData() {
    return this.properties.data;
  }

  setData(data) {
    this.properties.data = data;
    return this;
  }

  getHash() {
    if (!this.properties.hash) {
      this.properties.hash = this.messenger
        .createHash(this.properties.name, this.properties.data);
    }

    return this.properties.hash;
  }

  setHash(hash) {
    this.properties.hash = hash;
    return this;
  }

  listen(events, context) {
    this.messenger.emit('debug', this, 'listen', events, context);

    this.events.set(context, events);

    Object.keys(events).forEach((key) => {
      context.bindListener(key, this, events[key]);
    });

    return this;
  }

  unlisten(context) {
    this.messenger.emit('debug', this, 'unlisten', context);

    if (!this.events.has(context)) {
      return this;
    }

    const events = this.events.get(context);
    this.events.delete(context);

    Object.keys(events).forEach((key) => {
      context.unbindListener(key, this, events[key]);
    });

    return this;
  }

  bind() {
    this.messenger.emit('debug', this, 'bind');
    this.properties.bind = true;

    return this;
  }

  unbind() {
    this.messenger.emit('debug', this, 'unbind');
    this.properties.bind = false;

    return this;
  }

  data(data) {
    return this.setData(data);
  }

  validate() {}

  execute() {
    this.messenger.emit('debug', this, 'execute');

    return Promise.resolve()
      .then(this.validate.bind(this, this.properties.data))
      .then(this.handleExecute.bind(this))
      .catch(this.handleError.bind(this));
  }

  handleExecute() {
    this.messenger.emit('debug', this, 'handleExecute');
    this.messenger.send(this.properties.data, this, this.properties.bind);
  }

  handleResponse(response, request, message) {
    this.messenger.emit('debug', this, 'handleResponse',
      response, request, message);

    if (response.data.error) {
      return this.handleError(
        Error.fromJSON(response.data.error)
      );
    }

    return this.emit('data', response.data, response, message);
  }

  handleError(error) {
    this.emit('error', new Error('action_not_executed', {
      origin: error
    }));
  }
}

module.exports = AbstractAction;
