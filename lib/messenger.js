'use strict';

const ObjectHash = require('object-hash');
const Error = require('@scola/error');
const EventHandler = require('@scola/events');

class Messenger extends EventHandler {
  constructor() {
    super();

    this.id = 0;
    this.transport = null;
    this.requests = new Map();
  }

  open(transport) {
    this.emit('debug', this, 'open', transport);

    this.transport = transport;
    this.bindListeners();

    return this;
  }

  close() {
    this.emit('debug', this, 'close');
    this.unbindListeners();

    return this;
  }

  send(data, action, bind) {
    this.emit('debug', this, 'send', data, action, bind);

    const id = ++this.id;
    const request = {
      action,
      bind,
      data,
      id
    };

    request.message = this.sendRequest(request);
    this.requests.set(id, request);

    return this;
  }

  sendRequest(request) {
    const message = this.transport
      .createMessage()
      .set('data', request.data)
      .set('id', request.id)
      .set('name', request.action.getName());

    if (request.bind !== null) {
      message.set('bind', request.bind);
    }

    this.transport.send(message);

    return message;
  }

  deleteRequest(request) {
    this.emit('debug', this, 'deleteRequest', request);

    if (request.bind) {
      request.bind = false;
      this.sendMessage(request);
    }

    this.requests.delete(request.id);
    return this;
  }

  deleteAction(action) {
    this.emit('debug', this, 'deleteAction', action);

    const requests = new Map(this.requests);

    requests.forEach((request) => {
      if (request.action === action) {
        this.deleteRequest(request);
      }
    });
  }

  createHash(name, data) {
    this.emit('debug', this, 'createHash', name, data);

    return ObjectHash.sha1({
      name,
      data
    });
  }

  bindListeners() {
    this.bindListener('error', this.transport, this.handleError);
    this.bindListener('message', this.transport, this.handleMessage);
    this.bindListener('open', this.transport, this.handleOpen);
  }

  unbindListeners() {
    this.unbindListener('error', this.transport, this.handleError);
    this.unbindListener('message', this.transport, this.handleMessage);
    this.unbindListener('open', this.transport, this.handleOpen);
  }

  handleOpen() {
    this.emit('debug', this, 'handleOpen');

    const requests = new Map(this.requests);

    requests.forEach((request) => {
      if (request.bind) {
        this.sendRequest(request);
      } else {
        this.deleteRequest(request);
      }
    });
  }

  handleError(error) {
    this.emit('debug', this, 'handleError', error);

    if (error.message === 'transport_message_not_sent') {
      const request = this.requests.get(error.detail.message.body.id);

      this.requests.delete(request.id);
      this.transport.cancel(request.message);
    }
  }

  handleMessage(message) {
    this.emit('debug', this, 'handleMessage', message);

    try {
      this.handleResponse(message);
    } catch (error) {
      this.handleResponseError(message, error);
    }
  }

  handleResponse(message) {
    this.emit('debug', this, 'handleResponse', message);

    const response = message.getBody();
    response.data = response.data || {};

    if (!response.id) {
      throw new Error('response_invalid', {
        detail: {
          response
        }
      });
    }

    const request = this.requests.get(response.id);

    if (!request) {
      throw new Error('request_not_found', {
        detail: {
          response
        }
      });
    }

    if (!request.bind) {
      this.deleteRequest(request);
    }

    request.action.handleResponse(response, request, message);
  }

  handleResponseError(message, error) {
    this.emit('debug', this, 'handleResponseError', message, error);

    const request = this.requests.get(message.get('id'));

    if (request) {
      this.requests.delete(request.id);
      this.transport.cancel(request.message);
    }

    this.emit('error', new Error('action_message_not_handled', {
      origin: error,
      detail: {
        message
      }
    }));
  }
}

module.exports = Messenger;
